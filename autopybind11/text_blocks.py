# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

'''
Separate file for the text blocks to be formatted.
These strings will be formatted in generator.py, then used to
generate valid pybind11 binding code
'''


common_cpp_body = """
#include "pybind11/pybind11.h"
namespace py = pybind11;

{forwards}

PYBIND11_MODULE({name}, model)
{{
{init_funs}
}}
"""

init_fun_signature = """auto_bind_{name}(model);\n"""
init_fun_forward = """void auto_bind_{name}(py::module &model);\n"""


cppbody = """
#include "pybind11/operators.h"
#include "pybind11/pybind11.h"
#include "pybind11/stl.h"
{includes}


namespace py = pybind11;
//#include "drake/bindings/pydrake/documentation_pybind.h"
//#include "drake/bindings/pydrake/pydrake_pybind.h"

// namespace drake {{
// namespace pydrake {{
void auto_bind_{name}(py::module &m)
{{
  {class_info}
}}

// }}  // namespace pydrake
// }}  // namespace drake
"""
class_info_body = """
  py::class_<{pyclass_args}{no_delete}>(m, "{name}"{doc}, py::module_local())
    {constructor}
    {funcs}
    {vars}
    {opers}
    ;
"""
pybind_overload_macro_args = """{return_type},
      {parent_alias},
      {cpp_fxn_name},
      {arg_str}
"""

tramp_override = """{fxn_sig} override
  {{
    PYBIND11_OVERLOAD{pure}
    (
      {macro_args}
    );
  }}"""

trampoline_def = """class {tramp_name}
: public {class_decl}
{{
public:
  typedef {class_decl} {parent_alias};
  using {parent_alias}::{ctor_name};

  {virtual_overrides}
}};
"""

publicist_using_directives = """using {class_decl}::{fxn_name};"""

publicist_def = """class {publicist_name}
: public {class_decl}
{{
public:
  {using_directives}
}};
"""

class_module_cpp = """#include <pybind11/pybind11.h>
{includes}
{trampoline_str}
{publicist_str}
namespace py = pybind11;
void auto_bind_{namespace}(py::module &m)
{{
  {defs}
}}
"""

non_class_module_cpp = """#include <pybind11/pybind11.h>
{includes}
namespace py = pybind11;
void auto_bind_{namespace}(py::module &m)
{{
  {defs}
}}
"""
member_func = """{module}.def{static}(\"{fun_name}\", {fun_ref}{args}{doc}){ending}\n"""

constructor = """.def(py::init<{}>(){})\n"""
copy_constructor_tramp = """.def(py::init([]({tramp_arg_type} arg0) {{ return {trampname}(arg0); }}){doc})
{indent}.def(py::init([]({arg_type} arg0) {{ return {cname}(arg0); }}))\n"""
member_func_arg = """py::arg(\"{}\"){}"""
public_member_var = """.def_read{write}{static}(\"{var_name}\", {var_ref})\n"""
private_member_var = """.def_property{readonly}{static}(\"{var_name}\", {var_accessors})\n"""
member_reference = "&{classname}::{member}"
overload_template = """static_cast<{decl_string}>({fun_ref})"""
operator_template = """.def({arg1} {symbol} {arg2})\n"""
call_template = """.def("__call__", []({arg_str}){{ {fn_call} }})\n"""

wrap_header = """{includes}\n{class_decs}\n{func_ptr_assigns}"""
enum_header = """py::enum_<{class_name}>(m,\"{name}\",{type}, "{doc}")\n"""
enum_val = """.value(\"{short_name}\", {scoped_name}, \"{doc}\")\n"""
