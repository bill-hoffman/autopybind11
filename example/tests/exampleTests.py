# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import os
import sys
import unittest

# Make it so we search where we are running.
sys.path.append(os.getcwd())  # noqa

import example
# Import of test_base_double_py from additional causes conflict on run
# from additional.additional import nonTemplate_py


class exampleTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_test_base_double_py(self):
        tb = example.test_base_double_py()
        self.assertIn("base_adder", dir(tb))
        self.assertIn("base_var", dir(tb))
        tb.base_var = 4
        self.assertEqual(6, tb.base_adder(2))

    def test_test_double_py(self):
        # Test initializations
        t = example.test_double_py()
        self.assertEqual(4, t.adder(2, 2))
        self.assertAlmostEqual(4.5, t.adder(2, 2.5))
        self.assertEqual(9, t.summer(2, 2, 5))
        # test summer with defaults: <no default>, 0 , 14
        self.assertEqual(16, t.summer(2))

        with self.assertRaises(TypeError):
            # The final two parameters here are int, not float
            t.summer(1.0, 0, 1.0)

        # Test Inheritance
        t.base_var = 2
        self.assertEqual(4, t.base_adder(2))

        # Set/Get private variables
        t.priv_var1 = 2
        self.assertEqual(2, t.priv_var1)
        # Cannot set priv_var2, only read.
        with self.assertRaises(AttributeError):
            t.priv_var2 = 1
        print(t.priv_var2)

        # Test other initializations
        t1 = example.test_double_py(14)
        self.assertEqual(14, t1.val1_var)
        self.assertEqual(0, t1.val2_var)

        t2 = example.test_double_py(15, 20)
        self.assertEqual(15, t2.val1_var)
        self.assertEqual(0, t2.val2_var)

    def test_named_py(self):
        self.assertEqual(4, example.outside_adder_2(2, 2))

    def test_example_py(self):
        self.assertIn("template_f", dir(example))
        self.assertIn("template_f_multi_arg", dir(example))
        self.assertIn("multi_template", dir(example))
        self.assertIn("overloaded_non_template", dir(example))
        self.assertIn("no_namespace_adder", dir(example))

        # Test overloads of overloaded_non_template
        self.assertAlmostEqual(6, example.overloaded_non_template(3, 3))
        self.assertAlmostEqual(2, example.overloaded_non_template(2))
        self.assertAlmostEqual(0, example.overloaded_non_template())

        self.assertEqual(4, example.no_namespace_adder(2, 2))
        self.assertEqual(3, example.no_namespace_adder(2))
        self.assertEqual(1, example.no_namespace_adder())

    def test_example_enums(self):
        self.assertIn("namedType", dir(example))
        self.assertIn("unscopedType", dir(example))
        self.assertEqual(example.Four, example.unscopedType(0))
        self.assertEqual(example.Five, example.unscopedType(1))
        self.assertEqual(example.Six, example.unscopedType(2))
        self.assertEqual(example.One, example.namedType(0))
        self.assertEqual(example.Two, example.namedType(1))
        self.assertEqual(example.Three, example.namedType(2))

    def test_typedef_classes(self):
        self.assertIn("hour_duration_py", dir(example))
        self.assertIn("minute_duration_py", dir(example))
        hour = example.hour_duration_py()
        minute = example.minute_duration_py()
        self.assertEqual(0, hour.count())
        hour + 1
        self.assertEqual(1, hour.count())

        self.assertEqual(0, minute.count())
        minute + 1
        self.assertEqual(1, minute.count())

    def test_module_local(self):
        # Using py::module_local this will not error. Without it,
        # an ImportError will be raised.
        from additional import additional


if __name__ == '__main__':
    unittest.main()
