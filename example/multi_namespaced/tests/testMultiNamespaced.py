# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import os
import sys
import unittest

sys.path.append(os.getcwd())  # noqa

# TODO: this will need to be changed when modules follow the
# namespace hierarchy
import multi_namespaced_module as mnm

# Make it so we search where we are running.

# Import of test_base_double_py from additional causes conflict on run
# from additional.additional import nonTemplate_py


class multiNamespacedModuleTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.pi = 3.14
        cls.prec = 5

    @classmethod
    def tearDownClass(cls):
        pass

    def test_module_contents(self):
        # Test functions are present
        self.assertIn("no_nmspc_func", dir(mnm))
        self.assertIn("nmspc1_func", dir(mnm))
        self.assertIn("nmspc2_func", dir(mnm))
        self.assertIn("cmpnd_nmspc_func", dir(mnm))

    def test_multi_namespaced_module(self):
        double_float_class_inst = mnm.no_nmspc_class_double_float_py()
        int_float_class_inst = mnm.no_nmspc_class_int_float_py()
        fxn = mnm.no_nmspc_func
        self.check_module_class(double_float_class_inst, int_float_class_inst)
        self.check_module_fxn(fxn)

        double_float_class_inst = mnm.nmspc1_class_double_float_py()
        int_float_class_inst = mnm.nmspc1_class_int_float_py()
        fxn = mnm.nmspc1_func
        self.check_module_class(double_float_class_inst, int_float_class_inst)
        self.check_module_fxn(fxn)

        double_float_class_inst = mnm.nmspc2_class_double_float_py()
        int_float_class_inst = mnm.nmspc2_class_int_float_py()
        fxn = mnm.nmspc2_func
        self.check_module_class(double_float_class_inst, int_float_class_inst)
        self.check_module_fxn(fxn)

        double_float_class_inst = mnm.cmpnd_nmspc_class_double_float_py()
        int_float_class_inst = mnm.cmpnd_nmspc_class_int_float_py()
        fxn = mnm.cmpnd_nmspc_func
        self.check_module_class(double_float_class_inst, int_float_class_inst)
        self.check_module_fxn(fxn)

    def check_module_fxn(self, fxn):
        # Double and float instance
        self.assertAlmostEqual(fxn(0, 0), 0)
        self.assertAlmostEqual(fxn(3, 6), 9)
        self.assertAlmostEqual(fxn(3, self.pi), 3 + self.pi, self.prec)
        self.assertAlmostEqual(fxn(self.pi, 2 * self.pi), 3 * self.pi, self.prec)

    def check_module_class(self, double_float_class_inst, int_float_class_inst):
        # First test the class. Start with initial values
        self.assertEqual(double_float_class_inst.var1, 0)
        self.assertEqual(double_float_class_inst.var2, 0)
        self.assertEqual(int_float_class_inst.var1, 0)
        self.assertEqual(int_float_class_inst.var2, 0)

        # Check double_float instance first
        # First overload
        double_float_class_inst.update_vars(self.pi, 2 * self.pi)
        self.assertAlmostEqual(double_float_class_inst.var1, self.pi, self.prec)
        self.assertAlmostEqual(double_float_class_inst.var2, 2 * self.pi, self.prec)

        # Second overload
        double_float_class_inst.update_vars(4.5)
        self.assertAlmostEqual(double_float_class_inst.var1, self.pi + 4.5, self.prec)
        self.assertAlmostEqual(double_float_class_inst.var2, 2 * self.pi + 4.5, self.prec)

        # Now int_float instance
        # First overload
        int_float_class_inst.update_vars(3, 2 * self.pi)
        self.assertEqual(int_float_class_inst.var1, 3)
        self.assertAlmostEqual(int_float_class_inst.var2, 2 * self.pi, self.prec)

        # Second overload
        int_float_class_inst.update_vars(4)
        self.assertEqual(int_float_class_inst.var1, 7)
        self.assertAlmostEqual(int_float_class_inst.var2, 2 * self.pi + 4, self.prec)


if __name__ == '__main__':
    unittest.main()
