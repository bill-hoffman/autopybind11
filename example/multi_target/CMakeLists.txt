# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

# First target goes first
add_library(mt_target1 INTERFACE)
target_sources(mt_target1 INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/first_target_source1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/subdir/first_target_source2.cpp)

target_include_directories(mt_target1 INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/subdir)

target_compile_definitions(mt_target1 INTERFACE "-DFLAG1")


# Now the second target
add_library(mt_target2 INTERFACE)
target_sources(mt_target2 INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/second_target_source.hpp)

target_include_directories(mt_target2 INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

target_compile_definitions(mt_target2 INTERFACE "-DFLAG2")
target_compile_definitions(mt_target2 INTERFACE "-DFLAG3")

autopybind11_fetch_build_pybind11()

autopybind11_add_module("multi_target" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES mt_target1 mt_target2)


add_test(NAME multi_target_test
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/testMultiTarget.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

