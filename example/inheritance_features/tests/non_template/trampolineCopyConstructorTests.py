# Distributed under the OSI-approved BSD 3-Clause License.  See accompany_ing
# file Copyright.txt
import os
import sys
import unittest

from py_classes import *

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

class trampolineCCTests(unittest.TestCase):
    def configure_d1(self, d1=im.Derived1_py(), dfd1=DerivedFromD1()):
        d1.var1 = 1
        dfd1.var1 = 1
        return d1, dfd1

    def configure_d2(self, d2=im.Derived2_py(), dfd2=DerivedFromD2()):
        d2.var2 = 2
        dfd2.var2 = 2
        return self.configure_d1(d2, dfd2)

    def configure_ia(self, ia=im.InheritsAll_py(), dfia=DerivedFromIA()):
        ia.var3 = 3
        dfia.var3 = 3
        return self.configure_d2(ia, dfia)

    def check_d1(self, d1):
        self.assertEqual(d1.var1, 1)

    def check_d2(self, d2):
        self.assertEqual(d2.var2, 2)
        self.check_d1(d2)

    def check_ia(self, ia):
        self.assertEqual(ia.var3, 3)
        self.check_d2(ia)

    # Test each copy constructor works for cpp classes, including
    # those that have a class lower in the hierarchy as a parameter
    def test_cc_cpp(self):
        # D1 should accept D1's, D2's and IA's, and DerivedFromD1(DFD1), DFD2, DFIA
        ia_in, dfia_in = self.configure_ia()
        self.check_d1(im.Derived1_py(ia_in))
        self.check_d1(im.Derived1_py(dfia_in))
        d2_in, dfd2_in = self.configure_d2()
        self.check_d1(im.Derived1_py(d2_in))
        self.check_d1(im.Derived1_py(dfd2_in))
        d1_in, dfd1_in = self.configure_d1()
        self.check_d1(im.Derived1_py(d1_in))
        self.check_d1(im.Derived1_py(dfd1_in))

        # D2 should accept D2's, IA's, and DFD2, DFIA
        ia_in, dfia_in = self.configure_ia()
        self.check_d2(im.Derived2_py(ia_in))
        self.check_d2(im.Derived2_py(dfia_in))
        d2_in, dfd2_in = self.configure_d2()
        self.check_d2(im.Derived2_py(d2_in))
        self.check_d2(im.Derived2_py(dfd2_in))

        # IA should accept IA's, and DFIA
        ia_in, dfia_in = self.configure_ia()
        self.check_ia(im.InheritsAll_py(ia_in))
        self.check_ia(im.InheritsAll_py(dfia_in))

    # Test that copy constructors taking classes higher in the hierarchy
    # as a parameter do NOT work for cpp classes.
    def test_cc_fail_cpp(self):
        # D1 should not accept Base or DFB
        self.assertRaises(TypeError, im.Derived1_py, im.Base_py())
        self.assertRaises(TypeError, im.Derived1_py, DerivedFromBase())

        # D2 should not accept Base, D1, DFB, DFD1
        self.assertRaises(TypeError, im.Derived2_py, im.Base_py())
        self.assertRaises(TypeError, im.Derived2_py, DerivedFromBase())
        self.assertRaises(TypeError, im.Derived2_py, im.Derived1_py())
        self.assertRaises(TypeError, im.Derived2_py, DerivedFromD1())

        # IA should not accept Base, D1, D2, DFB, DFD1, DFD2
        self.assertRaises(TypeError, im.InheritsAll_py, im.Base_py())
        self.assertRaises(TypeError, im.InheritsAll_py, DerivedFromBase())
        self.assertRaises(TypeError, im.InheritsAll_py, im.Derived1_py())
        self.assertRaises(TypeError, im.InheritsAll_py, DerivedFromD1())
        self.assertRaises(TypeError, im.InheritsAll_py, im.Derived2_py())
        self.assertRaises(TypeError, im.InheritsAll_py, DerivedFromD2())

    # Test each copy constructor works for python classes, including
    # those that have a class lower in the hierarchy as a parameter
    def test_cc_py(self):
        # DFD1 should accept D1's, D2's and IA's, and DerivedFromD1(DFD1), DFD2, DFIA
        # Note that D1 is allowed,
        # since in the __init__ call for DFD1, we call D1's copy constructor
        # with a D1 as argument
        ia_in, dfia_in = self.configure_ia()
        self.check_d1(DerivedFromD1(ia_in))
        self.check_d1(DerivedFromD1(dfia_in))
        d2_in, dfd2_in = self.configure_d2()
        self.check_d1(DerivedFromD1(d2_in))
        self.check_d1(DerivedFromD1(dfd2_in))
        d1_in, dfd1_in = self.configure_d1()
        self.check_d1(DerivedFromD1(dfd1_in))
        self.check_d1(DerivedFromD1(d1_in))

        # DFD2 should accept D2's, IA's, DFD2, DFIA
        ia_in, dfia_in = self.configure_ia()
        self.check_d2(DerivedFromD2(ia_in))
        self.check_d2(DerivedFromD2(dfia_in))
        d2_in, dfd2_in = self.configure_d2()
        self.check_d2(DerivedFromD2(dfd2_in))
        self.check_d2(DerivedFromD2(d2_in))

        # DFIA should accept only DFIA and IA
        ia_in, dfia_in = self.configure_ia()
        self.check_ia(DerivedFromIA(dfia_in))
        self.check_ia(DerivedFromIA(ia_in))

    # Test that copy constructors taking classes higher in the hierarchy
    # as a parameter do NOT work for py classes.
    def test_cc_fail_py(self):
        # DFD1 should not accept Base, or DFB.
        self.assertRaises(TypeError, DerivedFromD1, im.Base_py())
        self.assertRaises(TypeError, DerivedFromD1, DerivedFromBase())

        # DFD2 should not accept Base, D1, DFB, DFD1
        self.assertRaises(TypeError, DerivedFromD2, im.Base_py())
        self.assertRaises(TypeError, DerivedFromD2, DerivedFromBase())
        self.assertRaises(TypeError, DerivedFromD2, im.Derived1_py())
        self.assertRaises(TypeError, DerivedFromD2, DerivedFromD1())

        # DFIA should not accept Base, D1, D2, DFB, DFD1, DFD2
        self.assertRaises(TypeError, DerivedFromIA, im.Base_py())
        self.assertRaises(TypeError, DerivedFromIA, DerivedFromBase())
        self.assertRaises(TypeError, DerivedFromIA, im.Derived1_py())
        self.assertRaises(TypeError, DerivedFromIA, DerivedFromD1())
        self.assertRaises(TypeError, DerivedFromIA, im.Derived2_py())
        self.assertRaises(TypeError, DerivedFromIA, DerivedFromD2())


if __name__ == '__main__':
    unittest.main()
