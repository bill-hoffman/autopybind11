# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# Classes that start with "DerivedFrom" are python classes that
# derived from a C++ class with bindings

class DerivedFromBase(im.Base_py):
    def __init__(self):
        im.Base_py.__init__(self) # Necessary for pybind

    # Override whoami
    def whoami(self):
        return "DerivedFromBase"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromBase.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromBase.priv_virt_fxn()"

class DerivedFromAbstractDerived1(im.AbstractDerived1_py):
    def __init__(self):
        im.AbstractDerived1_py.__init__(self) # Necessary for pybind

    # Override whoami
    def whoami(self):
        return "DerivedFromAbstractDerived1"

    # Override priv_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromAbstractDerived1.prot_virt_fxn()"

    def priv_virt_fxn(self):
        return "DerivedFromAbstractDerived1.priv_virt_fxn()"

class DerivedFromD1(im.Derived1_py):
    def __init__(self, other=None):
        if other is None:
            im.Derived1_py.__init__(self) # Necessary for pybind
        else:
            im.Derived1_py.__init__(self, other)

    # Override whoami, virt1
    def whoami(self):
        return "DerivedFromD1"

    def virt1(self, f):
        return "DerivedFromD1.virt1()"

    def prot_virt_fxn(self):
        return "DerivedFromD1.prot_virt_fxn()"

    def priv_virt_fxn(self):
        return "DerivedFromD1.priv_virt_fxn()"

class DerivedFromD2(im.Derived2_py):
    def __init__(self, other=None):
        if other is None:
            im.Derived2_py.__init__(self) # Necessary for pybind
        else:
            im.Derived2_py.__init__(self, other)

    # Override whoami
    def whoami(self):
        return "DerivedFromD2"

    # Override virt1-virt2
    def virt1(self, f):
        return "DerivedFromD2.virt1()"

    def virt2(self, f, s):
        return "DerivedFromD2.virt2()"

    def prot_virt_fxn(self):
        return "DerivedFromD2.prot_virt_fxn()"

    def priv_virt_fxn(self):
        return "DerivedFromD2.priv_virt_fxn()"

# InheritsAll doesn't add any new/implement virtual functions,
# but python classes should still be able to override inherited methods
class DerivedFromIA(im.InheritsAll_py):
    def __init__(self, other=None):
        if other is None:
            im.InheritsAll_py.__init__(self) # Necessary for pybind
        else:
            im.InheritsAll_py.__init__(self, other)

    def whoami(self):
        return "DerivedFromIA"

    # Override virt1-virt2
    def virt1(self, f):
        return "DerivedFromIA.virt1()"

    def virt2(self, f, s):
        return "DerivedFromIA.virt2()"

    def prot_virt_fxn(self):
        return "DerivedFromIA.prot_virt_fxn()"

    def priv_virt_fxn(self):
        return "DerivedFromIA.priv_virt_fxn()"
