# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import unittest
import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# Test that we can construct classes, set variables, etc...
class basicClassBehavior(unittest.TestCase):
    # This will fail if the classes aren't bound in the correct order.
    # InheritsAll inherits from Derived2
    # Derived2 inherits from Derived1,
    # Derived1 inherits from Base
    def test_order_correct(self):
        # First check Base is bound first
        im.Derived1_py()

        # Now check that Derived1 is bound before Derived2
        im.Derived2_py()

        # Now check that Derived2 is bound before InheritsAll
        im.InheritsAll_py()

    def test_construct(self):
        # First Derived1
        self.assertEqual(im.Derived1_py().var1, 0)

        # Now Derived2
        self.assertEqual(im.Derived2_py().var1, 0)
        self.assertEqual(im.Derived2_py().var2, 0)

        # Now InheritsAll
        self.assertEqual(im.InheritsAll_py().var1, 0)
        self.assertEqual(im.InheritsAll_py().var2, 0)
        self.assertEqual(im.InheritsAll_py().var3, 0)

    def test_get_set_vars(self):
        # First Derived1
        inst = im.Derived1_py()
        inst.var1 = 1
        self.assertEqual(inst.var1, 1)

        # Now Derived2
        inst = im.Derived2_py()
        inst.var1 = 1
        inst.var2 = 2
        self.assertEqual(inst.var1, 1)
        self.assertEqual(inst.var2, 2)

        # Now InheritsAll
        inst = im.InheritsAll_py()
        inst.var1 = 1
        inst.var2 = 2
        inst.var3 = 3
        self.assertEqual(inst.var1, 1)
        self.assertEqual(inst.var2, 2)
        self.assertEqual(inst.var3, 3)

    def test_normal_mthd_calls(self):
        # First Derived1
        self.assertEqual(im.Derived1_py().whoami(),    "Derived1")
        self.assertEqual(im.Derived1_py().virt1(6.28), "Derived1.virt1()")

        # Now Derived2
        self.assertEqual(im.Derived2_py().whoami(),    "Derived2")
        self.assertEqual(im.Derived2_py().virt2(-6.28, "py_string"), "Derived2.virt2()")

        # Now InheritsAll
        self.assertEqual(im.InheritsAll_py().nonvirtual(), "InheritsAll.nonvirtual()")

    # Not testing virtual-ness here, just that it was inherited
    # and can be called by the usual means. Notice that the implementation
    # was provided in Derived1
    def test_public_inherited_mthds(self):
        self.assertEqual(im.Derived2_py().virt1(6.28), "Derived1.virt1()")

        # Same for InheritsAll. All of the implementations should be the same as
        # in Derived2
        self.assertEqual(im.InheritsAll_py().whoami(),    "Derived2")
        self.assertEqual(im.InheritsAll_py().virt1(6.28), "Derived1.virt1()")
        self.assertEqual(im.InheritsAll_py().virt2(-6.28, "py_string"), "Derived2.virt2()")

    # Likewise with above, but with a protected member.
    # Should be "public" on the Python side. Implemented in Base class
    def test_prot_inherited_mthds(self):
        exp_string = "Base.prot_fxn()"
        self.assertEqual(im.Base_py().prot_fxn(), exp_string)
        self.assertEqual(im.AbstractDerived1_py().prot_fxn(), exp_string)
        self.assertEqual(im.Derived1_py().prot_fxn(), exp_string)
        self.assertEqual(im.Derived2_py().prot_fxn(), exp_string)
        self.assertEqual(im.InheritsAll_py().prot_fxn(), exp_string)

    # Also not testing virtual-ness here. Making sure that
    # subclasses can be substitute for a base class in a function argument
    # and that the correct behavior is observed
    def test_subclass_as_argument(self):
        # First check D2 and IA inherit virt1 and its implementation
        # and can be called from reference to base
        derived1_exp_string = "Derived1: Derived1.virt1()"
        derived2_exp_string = "Derived2: Derived1.virt1()"
        self.assertEqual(im.call_virt_from_derived1(im.Derived1_py()), derived1_exp_string)
        self.assertEqual(im.call_virt_from_derived1(im.Derived2_py()), derived2_exp_string)
        self.assertEqual(im.call_virt_from_derived1(im.InheritsAll_py()), im.call_virt_from_derived1(im.Derived2_py()))

        # Now check same thing for virt2 in D2 and IA
        derived2_exp_string = "Derived2: Derived1.virt1()Derived2.virt2()"
        self.assertEqual(im.call_virt_from_derived2(im.Derived2_py()), derived2_exp_string)
        self.assertEqual(im.call_virt_from_derived2(im.InheritsAll_py()), im.call_virt_from_derived2(im.Derived2_py()))

    def test_inheritance_relationships(self):
        self.assertTrue(issubclass(im.InheritsAll_py, im.Derived2_py))
        self.assertTrue(issubclass(im.InheritsAll_py, im.Derived1_py))
        self.assertTrue(issubclass(im.InheritsAll_py, im.Base_py))

        self.assertTrue(issubclass(im.Derived2_py, im.Derived1_py))
        self.assertTrue(issubclass(im.Derived2_py, im.Base_py))

        self.assertTrue(issubclass(im.Derived1_py, im.Base_py))

if __name__ == '__main__':
    unittest.main()

