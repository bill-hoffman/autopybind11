# Distributed under the OSI-approved BSD 3-Clause License.  See accompany_ing
# file Copyright.txt
import os
import sys
import unittest

from py_classes import *

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# dftd[number] is short for "DerivedFromTemplatedDerived[number]"

class trampolineCCTests(unittest.TestCase):
    def configure_td1(self, td1=im.TD1_float_py(), dftd1=DerivedFromTD1Float()):
        td1.var1 = 1
        dftd1.var1 = 1
        return td1, dftd1

    def configure_td2(self, td2=im.TD2_float_double_py(), dftd2=DerivedFromTD2FloatDouble()):
        td2.var2 = 2
        dftd2.var2 = 2
        return self.configure_td1(td2, dftd2)

    def check_td1(self, td1):
        self.assertEqual(td1.var1, 1)

    def check_td2(self, td2):
        self.assertEqual(td2.var2, 2)
        self.check_td1(td2)

    # Test each copy constructor works for cpp classes, including
    # those that have a class lower in the hierarchy as a parameter
    def test_cc_cpp(self):
        # TD1 should accept TD1's, TD2's, and DerivedFromTD1(DFTD1), DFTD2
        td2_in, dftd2_in = self.configure_td2()
        self.check_td1(im.TD1_float_py(td2_in))
        self.check_td1(im.TD1_float_py(dftd2_in))
        td1_in, dftd1_in = self.configure_td1()
        self.check_td1(im.TD1_float_py(td1_in))
        self.check_td1(im.TD1_float_py(dftd1_in))

        # TD2 should accept TD2's and DFTD2
        td2_in, dftd2_in = self.configure_td2()
        self.check_td2(im.TD2_float_double_py(td2_in))
        self.check_td2(im.TD2_float_double_py(dftd2_in))

    # Test that copy constructors taking classes higher in the hierarchy
    # as a parameter do NOT work for cpp classes.
    def test_cc_fail_cpp(self):
        # TD1 should not accept Base
        self.assertRaises(TypeError, im.TD1_float_py, im.Base_py())

        # TD2 should not accept Base, TD1, DFTD1
        self.assertRaises(TypeError, im.TD2_float_double_py, im.Base_py())
        self.assertRaises(TypeError, im.TD2_float_double_py, im.TD1_float_py())
        self.assertRaises(TypeError, im.TD2_float_double_py, DerivedFromTD1Float())

    # Test each copy constructor works for python classes, including
    # those that have a class lower in the hierarchy as a parameter
    def test_cc_py(self):
        # DFTD1 should accept TD2's, and DerivedFromTD1(DFTD1), DFTD2.
        # Note that TD1 is allowed,
        # since in the __init__ call for DFTD1, we call TD1's copy constructor
        # with a TD1 as argument
        td2_in, dftd2_in = self.configure_td2()
        self.check_td1(DerivedFromTD1Float(td2_in))
        self.check_td1(DerivedFromTD1Float(dftd2_in))
        td1_in, dftd1_in = self.configure_td1()
        self.check_td1(DerivedFromTD1Float(dftd1_in))
        self.check_td1(DerivedFromTD1Float(td1_in))

        # DFTD2 should accept TD2 and DFTD2
        td2_in, dftd2_in = self.configure_td2()
        self.check_td2(DerivedFromTD2FloatDouble(dftd2_in))
        self.check_td2(DerivedFromTD2FloatDouble(td2_in))

    # Test that copy constructors taking classes higher in the hierarchy
    # as a parameter do NOT work for py classes.
    def test_cc_fail_py(self):
        # DFTD1 should not accept Base.
        self.assertRaises(TypeError, DerivedFromTD1Float, im.Base_py())

        # DFTD2 should not accept Base, TD1, DFTD1
        self.assertRaises(TypeError, DerivedFromTD2FloatDouble, im.Base_py())
        self.assertRaises(TypeError, DerivedFromTD2FloatDouble, im.TD1_float_py())
        self.assertRaises(TypeError, DerivedFromTD2FloatDouble, DerivedFromTD1Float())

if __name__ == '__main__':
    unittest.main()
