# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import unittest
import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# Not going to test all of the same functionality as in non_template/basicClassTests,
# as basic class functionality has been tested thoroughly elsewhere in the examples.
# Just test new functionality not tested elsewhere
class basicClassBehavior(unittest.TestCase):
    # Should be "public" on the Python side. Implemented in Base class
    def test_prot_inherited_mthds(self):
        exp_string = "Base.prot_fxn()"
        self.assertEqual(im.TAD1_float_py().prot_fxn(), exp_string)
        self.assertEqual(im.TAD1_int_py().prot_fxn(), exp_string)
        self.assertEqual(im.TD1_float_py().prot_fxn(), exp_string)
        self.assertEqual(im.TD1_int_py().prot_fxn(), exp_string)
        self.assertEqual(im.TD2_float_double_py().prot_fxn(), exp_string)
        self.assertEqual(im.TD2_int_double_py().prot_fxn(), exp_string)

if __name__ == '__main__':
    unittest.main()

