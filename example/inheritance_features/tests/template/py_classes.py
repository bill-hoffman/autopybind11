# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

import sys
import os

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# Classes that start with "DerivedFrom" are python classes that
# derived from a C++ class with bindings

class DerivedFromTAD1Float(im.TAD1_float_py):
    def __init__(self):
        im.TAD1_float_py.__init__(self) # Necessary for pybind

    # Override whoami
    def whoami(self):
        return "DerivedFromTAD1Float"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTAD1Float.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTAD1Float.priv_virt_fxn()"

class DerivedFromTAD1Int(im.TAD1_int_py):
    def __init__(self):
        im.TAD1_int_py.__init__(self) # Necessary for pybind

    # Override whoami
    def whoami(self):
        return "DerivedFromTAD1Int"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTAD1Int.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTAD1Int.priv_virt_fxn()"

class DerivedFromTD1Float(im.TD1_float_py):
    def __init__(self, other=None):
        if other is None:
            im.TD1_float_py.__init__(self) # Necessary for pybind
        else:
            im.TD1_float_py.__init__(self, other)

    # Override whoami
    def whoami(self):
        return "DerivedFromTD1Float"

    # Override virt1
    def virt1(self, f):
        return "DerivedFromTD1Float.virt1()"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTD1Float.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTD1Float.priv_virt_fxn()"

class DerivedFromTD1Int(im.TD1_int_py):
    def __init__(self, other=None):
        if other is None:
            im.TD1_int_py.__init__(self) # Necessary for pybind
        else:
            im.TD1_int_py.__init__(self, other)

    # Override whoami
    def whoami(self):
        return "DerivedFromTD1Int"

    # Override virt1
    def virt1(self, f):
        return "DerivedFromTD1Int.virt1()"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTD1Int.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTD1Int.priv_virt_fxn()"

class DerivedFromTD2FloatDouble(im.TD2_float_double_py):
    def __init__(self, other=None):
        if other is None:
            im.TD2_float_double_py.__init__(self) # Necessary for pybind
        else:
            im.TD2_float_double_py.__init__(self, other)

    # Override whoami
    def whoami(self):
        return "DerivedFromTD2FloatDouble"

    # Override virt1
    def virt1(self, f):
        return "DerivedFromTD2FloatDouble.virt1()"

     # Override virt2
    def virt2(self, f, s):
        return "DerivedFromTD2FloatDouble.virt2()"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTD2FloatDouble.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTD2FloatDouble.priv_virt_fxn()"

class DerivedFromTD2IntDouble(im.TD2_int_double_py):
    def __init__(self, other=None):
        if other is None:
            im.TD2_int_double_py.__init__(self) # Necessary for pybind
        else:
            im.TD2_int_double_py.__init__(self, other)

    # Override whoami
    def whoami(self):
        return "DerivedFromTD2IntDouble"

    # Override virt1
    def virt1(self, f):
        return "DerivedFromTD2IntDouble.virt1()"

    # Override virt2
    def virt2(self, f, s):
        return "DerivedFromTD2IntDouble.virt2()"

    # Override prot_virt_fxn
    def prot_virt_fxn(self):
        return "DerivedFromTD2IntDouble.prot_virt_fxn()"

    # Override priv_virt_fxn
    def priv_virt_fxn(self):
        return "DerivedFromTD2IntDouble.priv_virt_fxn()"
