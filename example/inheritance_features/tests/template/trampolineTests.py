# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt
import unittest
import sys
import os

from py_classes import *

# Make it so we search where we are running.
sys.path.append(os.getcwd())

import inheritance_module as im

# Test that trampolines give us the expected functionality.
# Virtual-ness should be honored by Python for both C++ classes exposed in Python,
# and for Python classes inheriting from C++ classes
class trampolines(unittest.TestCase):
    def test_public_virt_overrides_cpp(self):
        # First call whoami from base reference. Downcasting should ensure that
        # this always prints the lowest implementation in the inheritance tree
        # Method isn't implemented in abstract class, so this should raise an exception
        self.assertRaises(RuntimeError, im.TAD1_float_py().whoami)
        self.assertRaises(RuntimeError, im.TAD1_int_py().whoami)

        # Now the concrete classes
        self.assertEqual(im.call_virt_whoami(im.TD1_float_py()), "TD1")
        self.assertEqual(im.call_virt_whoami(im.TD1_int_py()),   "TD1")
        self.assertEqual(im.call_virt_whoami(im.TD2_float_double_py()), "TD2")
        self.assertEqual(im.call_virt_whoami(im.TD2_int_double_py()),   "TD2")

    def test_prot_virt_overrides_cpp(self):
        # Not implemented in abstract class
        self.assertRaises(RuntimeError, im.TAD1_float_py().call_prot_virt_fxn)
        self.assertRaises(RuntimeError, im.TAD1_int_py().call_prot_virt_fxn)

        # TD1 sets the implementation, everyone else inherits
        self.assertEqual(im.TD1_float_py().call_prot_virt_fxn(), "TD1.prot_virt_fxn()")
        self.assertEqual(im.TD1_int_py().call_prot_virt_fxn(), "TD1.prot_virt_fxn()")
        self.assertEqual(im.TD2_float_double_py().call_prot_virt_fxn(), "TD1.prot_virt_fxn()")
        self.assertEqual(im.TD2_int_double_py().call_prot_virt_fxn(), "TD1.prot_virt_fxn()")

    def test_priv_virt_overrides_cpp(self):
        # Not implemented in abstract class
        self.assertRaises(RuntimeError, im.TAD1_float_py().call_priv_virt_fxn)
        self.assertRaises(RuntimeError, im.TAD1_int_py().call_priv_virt_fxn)

        # TD1 sets the implementation, everyone else inherits
        self.assertEqual(im.TD1_float_py().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")
        self.assertEqual(im.TD1_int_py().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")
        self.assertEqual(im.TD2_float_double_py().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")
        self.assertEqual(im.TD2_int_double_py().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")

    def test_functions_present(self):
        self.assertIn("call_virt_whoami",   dir(im))
        self.assertIn("call_virt_from_td1", dir(im))
        self.assertIn("call_virt_from_td2", dir(im))

    # Test virtual functions can be overriden in python
    def test_public_virt_overrides_py(self):
        # Check that virtual methods can be accessed
        # from a reference to any base for a python class

        # Both instances of our abstract templated class
        exp_string = "DerivedFromTAD1Float"
        self.assertEqual(im.call_virt_whoami(DerivedFromTAD1Float()), exp_string)

        exp_string = "DerivedFromTAD1Int"
        self.assertEqual(im.call_virt_whoami(DerivedFromTAD1Int()), exp_string)

        # Check python classes for both instances of td1
        # td1_float:
        exp_string = "DerivedFromTD1Float"
        self.assertEqual(im.call_virt_whoami(DerivedFromTD1Float()), exp_string)

        exp_string += ": DerivedFromTD1Float.virt1()"
        self.assertEqual(im.call_virt_from_td1(DerivedFromTD1Float()), exp_string)

        # td1_int:
        exp_string = "DerivedFromTD1Int"
        self.assertEqual(im.call_virt_whoami(DerivedFromTD1Int()), exp_string)

        exp_string += ": DerivedFromTD1Int.virt1()"
        self.assertEqual(im.call_virt_from_td1(DerivedFromTD1Int()), exp_string)

        # Now td2, starting with td2_float_double:
        exp_string = "DerivedFromTD2FloatDouble"
        self.assertEqual(im.call_virt_whoami(DerivedFromTD2FloatDouble()), exp_string)

        exp_string += ": DerivedFromTD2FloatDouble.virt1()"
        self.assertEqual(im.call_virt_from_td1(DerivedFromTD2FloatDouble()), exp_string)

        exp_string += "DerivedFromTD2FloatDouble.virt2()"
        self.assertEqual(im.call_virt_from_td2(DerivedFromTD2FloatDouble()), exp_string)

        # td2_int_double:
        exp_string = "DerivedFromTD2IntDouble"
        self.assertEqual(im.call_virt_whoami(DerivedFromTD2IntDouble()), exp_string)

        exp_string += ": DerivedFromTD2IntDouble.virt1()"
        self.assertEqual(im.call_virt_from_td1(DerivedFromTD2IntDouble()), exp_string)

        exp_string += "DerivedFromTD2IntDouble.virt2()"
        self.assertEqual(im.call_virt_from_td2(DerivedFromTD2IntDouble()), exp_string)

    def test_prot_virt_overrides_py(self):
        # Check python classes deriving from C++ abstract class
        exp_string = "DerivedFromTAD1Float.prot_virt_fxn()"
        self.assertEqual(DerivedFromTAD1Float().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromTAD1Int.prot_virt_fxn()"
        self.assertEqual(DerivedFromTAD1Int().call_prot_virt_fxn(), exp_string)

        # Now classes derived from concrete C++ classes
        exp_string = "DerivedFromTD1Float.prot_virt_fxn()"
        self.assertEqual(DerivedFromTD1Float().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromTD1Int.prot_virt_fxn()"
        self.assertEqual(DerivedFromTD1Int().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromTD2FloatDouble.prot_virt_fxn()"
        self.assertEqual(DerivedFromTD2FloatDouble().call_prot_virt_fxn(), exp_string)

        exp_string = "DerivedFromTD2IntDouble.prot_virt_fxn()"
        self.assertEqual(DerivedFromTD2IntDouble().call_prot_virt_fxn(), exp_string)

    def test_priv_virt_overrides_py(self):
        # Check python classes deriving from C++ abstract class
        exp_string = "DerivedFromTAD1Float.priv_virt_fxn()"
        self.assertEqual(DerivedFromTAD1Float().call_priv_virt_fxn(), exp_string)

        exp_string = "DerivedFromTAD1Int.priv_virt_fxn()"
        self.assertEqual(DerivedFromTAD1Int().call_priv_virt_fxn(), exp_string)

        # These will currently fail. Private virtual functions with an implementation
        # aren't allowed in the trampoline, so python subclasses should not be able to override them.
        # They will instead use the most recent implementation in the derivation tree on the C++ side
        # DFTD1 and DFTD2 implement the function, but C++ fails to recognize this new implementation
        self.assertEqual(DerivedFromTD1Float().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")
        self.assertEqual(DerivedFromTD1Int().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")

        self.assertEqual(DerivedFromTD2FloatDouble().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")
        self.assertEqual(DerivedFromTD2IntDouble().call_priv_virt_fxn(), "TD1.priv_virt_fxn()")


if __name__ == '__main__':
    unittest.main()
