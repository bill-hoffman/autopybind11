# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

add_library(inheritance INTERFACE)
target_sources(inheritance  INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/base.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/abstract_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/derived2.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/non_template/inherits_all.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_abstract_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_derived1.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/template/tmplt_derived2.hpp)

target_include_directories(inheritance INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

autopybind11_fetch_build_pybind11()

autopybind11_add_module("inheritance_module"
                       YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                       CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                       DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                       LINK_LIBRARIES inheritance
                       )

# Non-templated tests
add_test(NAME non_template_basic_behavior_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/non_template/basicClassTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_test(NAME non_template_trampoline_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/non_template/trampolineTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_test(NAME non_template_tramp_copy_constructor_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/non_template/trampolineCopyConstructorTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

# Templated test
add_test(NAME template_basic_behavior_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/template/basicClassTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_test(NAME template_trampoline_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/template/trampolineTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_test(NAME template_tramp_copy_constructor_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/template/trampolineCopyConstructorTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
