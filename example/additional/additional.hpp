/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt                                                      */
#ifndef ADDITIONAL_TEST_HPP
#define ADDITIONAL_TEST_HPP


template < typename T = double >
class adl_test_base
{
public:
  T base_var;
  inline adl_test_base() {};
  inline T mult(const T& val) {return base_var * val; };
};

class nonTemplate
{
public:
  nonTemplate();
  double div(double v1, double v2);
  float fdiv(float v1, float v2);
};

#endif
