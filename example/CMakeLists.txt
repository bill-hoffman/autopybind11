# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt

set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS TRUE)
add_library(wrapper_example INTERFACE)
target_sources(wrapper_example INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR}/test.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/test_2.hpp)
target_include_directories(wrapper_example INTERFACE
  ${CMAKE_CURRENT_SOURCE_DIR})

option(DEFINE_TEST_PARAM1 "Adds a define parameter to change whether flag1_set function is defined" OFF)
if (DEFINE_TEST_PARAM1)
  target_compile_definitions(wrapper_example PUBLIC "-DTESTFLAG1")
endif()

option(DEFINE_TEST_PARAM2 "Adds a define parameter to change whether flag2_set function is defined" OFF)
if (DEFINE_TEST_PARAM2)
  target_compile_definitions(wrapper_example PUBLIC "-DTESTFLAG2")
endif()

autopybind11_fetch_build_pybind11()

# Create default library
add_subdirectory(additional)
add_subdirectory(multi_namespaced)
add_subdirectory(multi_target)
add_subdirectory(operator_testing)
add_subdirectory(inheritance_features)

autopybind11_add_module("example" YAML_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/wrapper_input.yml
                        CONFIG_INPUT ${CMAKE_CURRENT_SOURCE_DIR}/config.yml
                        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
                        LINK_LIBRARIES wrapper_example)

# Create default tests, importing the module
set(tgt_helper_name "example")
set(py_command "import ${tgt_helper_name}")
add_test(NAME test_import_${tgt_helper_name}
         COMMAND ${PYTHON_EXECUTABLE} -c ${py_command}
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
add_test(NAME example_unittest
         COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/tests/exampleTests.py
         WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
