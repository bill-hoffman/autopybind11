$erroractionpreference = "stop"

$version = "3.7.7"
$sha256sum = "705c03140cfd3372f27ee911db5f4eb1fc9b980c9e27544adbd1a6adf942a1b0"
$filename = "python-$version-embed-amd64"
$tarball = "$filename.zip"

$outdir = $pwd.Path
$outdir = "$outdir\.gitlab"
Invoke-WebRequest -Uri "https://www.python.org/ftp/python/$version/$tarball" -OutFile "$outdir\$tarball"
$hash = Get-FileHash "$outdir\$tarball" -Algorithm SHA256
if ($hash.Hash -ne $sha256sum) {
    exit 1
}

Add-Type -AssemblyName System.IO.Compression.FileSystem
[System.IO.Compression.ZipFile]::ExtractToDirectory("$outdir\$tarball", "$outdir\python")
New-Item -ItemType directory "$outdir\python\DLLs"